class Hangman
  attr_reader :guesser, :referee, :board

  HANGMAN = '|---<o-|-<8'.freeze

  def initialize(players)
    @referee = players[:referee]
    @guesser = players[:guesser]
    setup
  end

  def setup
    word_length = @referee.pick_secret_word
    @guesser.register_secret_length(word_length)
    @board = ['_'] * word_length
    @wrong = []
  end

  def take_turn
    display
    guess = @guesser.guess(board)
    hits = @referee.check_guess(guess)
    @guesser.handle_response(guess, hits)
    update_board(guess, hits)
  end

  def update_board(guess, hits)
    hits.each { |i| @board[i] = guess }
    @wrong << guess if hits.empty?
  end

  def display
    puts @board.join(' ')
    puts "Wrong: [#{@wrong.join(' ')}]"
    puts HANGMAN[0..@wrong.length + 3]
    puts
  end

  def win?
    !board.include?('_')
  end

  def lose?
    @wrong.length == HANGMAN.length - 4
  end

  def play
    take_turn until win? || lose?
    display
    puts win? ? 'Guesser wins!' : 'He dead! Guesser loses.'
  end
end

class HumanPlayer
  def pick_secret_word
    print 'How many letters is the word? '
    gets.to_i
  end

  def guess(_board)
    print 'Guess a letter: '
    gets.chomp
  end

  def handle_response(guess, hits); end

  def register_secret_length(word_length); end

  def check_guess(letter)
    print "Which letters are #{letter}? (e.g. 1 4 5, or press enter if none). "
    gets.chomp.split.map { |char| char.to_i - 1 }
  end
end

class ComputerPlayer
  attr_reader :secret_word, :candidate_words

  def initialize(dictionary)
    @dictionary = dictionary
    @candidate_words = @dictionary.dup
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.length
  end

  def guess(board)
    freq = Hash.new(0)
    @candidate_words.join.chars.each do |char|
      freq[char] += 1 unless board.include?(char)
    end
    freq.max_by { |_k, v| v }[0]
  end

  def handle_response(guess, hits)
    @candidate_words.select! do |word|
      word.chars.each_index.all? do |i|
        hits.include?(i) ? word[i] == guess : word[i] != guess
      end
    end
  end

  def register_secret_length(word_length)
    @candidate_words.select! { |word| word.length == word_length }
  end

  def check_guess(letter)
    @secret_word.chars.each_index.select { |i| @secret_word[i] == letter }
  end
end

dictionary = File.read('lib/dictionary.txt').split($RS)
computer_player = ComputerPlayer.new(dictionary)
human_player = HumanPlayer.new
referee = ARGV.length.zero? ? computer_player : human_player
guesser = ARGV.length.zero? ? human_player : computer_player
ARGV.clear
game = Hangman.new(referee: referee, guesser: guesser)
game.play
